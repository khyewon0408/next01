"use client";
import Link from "next/link";
import styles from "../styles/movie.module.css"
import { useRouter } from "next/navigation";

interface IMovieProps {
    title: string;
    id: string;
    poster_path: string;
  }


export default function movie({id,poster_path, title}:IMovieProps){
    const router =useRouter();
    const onClick = ()=>{
        router.push(`/movie/${id}`);
    }
    return <div className={styles.movie} key={id}>
    <img src={poster_path} alt={title} onClick={onClick}/>
    <Link prefetch href={`/movie/${id}`}>{title}</Link>
</div>
}